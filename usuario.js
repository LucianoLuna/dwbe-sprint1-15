class Usuario {
    constructor(nombre, apellido, email, pais, clave, claveRepetida) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.pais = pais;
        this.clave = clave;
        this.claveRepetida = claveRepetida;
    }
}